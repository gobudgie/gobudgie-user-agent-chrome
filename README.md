# <img src="gobudgie_128.png" width="45" align="left"> Gobudgie User Agent

Google Chrome Extension that modifies User-Agent string in outgoing requests to contain the name of Linux based distribution Gobudgie.

You can get if from [Google Chrome Store](https://chrome.google.com/webstore/detail/gobudgie-user-agent/bljjpjmhngchblkpfekekaeohhbojcki).

## Testing

###### Chrome

1. Clone or [download](https://github.com/JasMich/gobudgie-user-agent-chrome/archive/master.zip) project;

2. Navigate to `chrome://extensions`

3. Click on `Load unpacked extension...`

4. Select the `gobudgie-user-agent-chrome` folder

## Contributing

If you want to help, please read the [Contributing](/CONTRIBUTING.md) guide.

## History

For detailed changelog, see [Releases](https://github.com/gobudgie/gobudgie-user-agent-chrome/releases).
